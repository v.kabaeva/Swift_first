import Foundation

// Задача 1
struct Candidate {
    let grade: Grade
    let requiredSalary: Int
    let fullName: String
    
    enum Grade {
        case junior
        case middle
        case senior
    }
}

protocol CandidateFilter {
    func filter(_ candidates: [Candidate]) -> [Candidate]
}

struct FilterByGrade: CandidateFilter {
    private let grade: Candidate.Grade
    
    init(grade: Candidate.Grade) {
        self.grade = grade
    }
    
    func filter(_ candidates: [Candidate]) -> [Candidate] {
        candidates.filter { $0.grade == grade }
    }
}

struct FilterBySalary: CandidateFilter {
    private let salary: Int
    
    init(salary: Int) {
        self.salary = salary
    }
    
    func filter(_ candidates: [Candidate]) -> [Candidate] {
        candidates.filter { $0.requiredSalary <= salary }
    }
}

struct FilterByFullName: CandidateFilter {
    private let name: String
    
    init(name: String) {
        self.name = name
    }

    func filter(_ candidates: [Candidate]) -> [Candidate] {
        candidates.filter { $0.fullName.contains(name) }
    }
}

let listOfCandidates: [Candidate] = [
    Candidate(grade: .junior, requiredSalary: 100000, fullName: "Джон Сноу"),
    Candidate(grade: .middle, requiredSalary: 200000, fullName: "Стив Джобс"),
    Candidate(grade: .senior, requiredSalary: 300000, fullName: "Вася Пупкин")
]

func filteredResult(defaultMessage: String, array: [Candidate]) {
    var result = [String]()
    
    for i in 0..<array.count {
        result.append(array[i].fullName)
    }
    print(defaultMessage, result)
}

let filterResultByGrade = FilterByGrade(grade: .middle).filter(listOfCandidates)
filteredResult(defaultMessage: "Фильтрация по грейду:", array: filterResultByGrade)

let filterResultBySalary = FilterBySalary(salary: 250000).filter(listOfCandidates)
filteredResult(defaultMessage: "Фильтрация по зарплате:", array: filterResultBySalary)

let filterResultByName = FilterByFullName(name: "Вася").filter(listOfCandidates)
filteredResult(defaultMessage: "Фильтрация по имени:", array: filterResultByName)

//Задача 2
extension Candidate.Grade {
    var order: Int {
        switch self {
        case .junior: return 1
        case .middle: return 2
        case .senior: return 3
        }
    }
}

struct FilterByMinGrade: CandidateFilter {
    let minGrade: Candidate.Grade
    
    func filter(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter {
            $0.grade.order >= minGrade.order
        }
    }
}

let filterResultsByMinGrade = FilterByMinGrade(minGrade: .middle).filter(listOfCandidates)

filteredResult(defaultMessage: "Фильтрация по минимальному грейду:", array: filterResultsByMinGrade)
