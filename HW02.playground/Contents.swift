import Foundation

//Задача 1
let milkmanPhrase = "Молоко - это полезно"
print("Задача 1:\n" + milkmanPhrase + "\n")

//Задача 2
let milkPriceOLD: Int = 3
print("Задача 2:\nСтарая цена =", String(milkPriceOLD), "\n")

//Задача 3
var milkPrice: Double = 3.0
milkPrice = 4.2
print("Задача 3:\nНовая цена = \(milkPrice)\n")

//Задача 4
var milkBottleCount: Int?
var profit: Double = 0.0

milkBottleCount = 20
profit = milkPrice * Double(milkBottleCount!)

print("Задача 4:\nВыручка =", String(profit), "\n")

// Задача 4*
// Без использования безопасного развертывания программа прекратит выполнение, если optionalType-переменная будет nil.
if let safeMilkBottleCount = milkBottleCount {
    profit = milkPrice * Double(safeMilkBottleCount)
    print ("Задача 4*:\nВыручка =", String(profit), "\n")
} else {
    print ("Задача 4*:\nОшибка! Нет бутылок в кузове\n")
}

//Задача 5
var employeesList = [String]()
employeesList += ["Иван", "Марфа", "Андрей", "Пётр", "Геннадий"]
print("Задача 5:\nСписок сотрудников:", employeesList, "\n")

//Задача 6
print("Задача 6:")
func checkWorkingTime(workingHours: Int) {
    var isEmployeeWorkHard: Bool = false
    if workingHours >= 40 {
        isEmployeeWorkHard = true
    }
    print("Cотрудник отработал", String(workingHours), "часов")
    print("Сотрудник работает добросовестно? -", isEmployeeWorkHard ? "Да" : "Нет")
}
checkWorkingTime(workingHours: 40)
checkWorkingTime(workingHours: 39)
