import Foundation

// Задача 1
class NameList {
    var names: [Character: [String]] = [:]
    
    func addName(name: String) {
        let firstLetter = name.first ?? " "
        
        if names[firstLetter] != nil {
            names[firstLetter]?.append(name)
        } else {
            names[firstLetter] = [name]
        }
    }
    
    func printNames() {
        let sortedKeys = names.keys.sorted()
        
        for key in sortedKeys {
            print("\(key)")
            let sortedNames = names[key]?.sorted() ?? []
            for name in sortedNames {
                print(name)
            }
        }
    }
}

let list = NameList()
list.addName(name: "Акулова")
list.addName(name: "Валерьянов")
list.addName(name: "Бочкин")
list.addName(name: "Бабочкина")
list.addName(name: "Арбузов")
list.addName(name: "Васильева")

list.printNames()
