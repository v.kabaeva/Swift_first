import Foundation

// Задача 1
func makeBuffer() -> (String) -> Void {
    var storage = ""
    
    return { addingText in
        if addingText.isEmpty {
            print(storage)
        } else {
            storage += addingText
        }
    }
}

let buffer = makeBuffer()

buffer("Задача 1:\n")
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!\n")
buffer("") // Замыкание использовать нужно!

// Задача 2
func checkPrimeNumber(_ number: Int) -> Bool {
    // здесь достаточно проверить, что чисто больше единицы 
    guard number > 1 else { return false }

    for i in 2..<number {
        if number % i == 0 {
            return false
        }
    }
    
    return true
}

checkPrimeNumber(7) // true
checkPrimeNumber(8) // false
checkPrimeNumber(13) // true
checkPrimeNumber(0) // false
checkPrimeNumber(2) //true
