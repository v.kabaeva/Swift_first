import Foundation

// Задача 1
class Shape {
    func calculateArea() -> Double {
        fatalError("not implemented")
    }

    func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

final class Rectangle: Shape {
    private let width: Double
    private let height: Double

    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }

    override func calculateArea() -> Double {
        width * height
    }

    override func calculatePerimeter() -> Double {
        2 * (width + height)
    }
}

final class Circle: Shape {
    private let radius: Double

    init(radius: Double) {
        self.radius = radius
    }

    override func calculateArea() -> Double {
        Double.pi * pow(radius, 2)
    }

    override func calculatePerimeter() -> Double {
        2 * Double.pi * radius
    }
}

final class Square: Shape {
    private let side: Double

    init(side: Double) {
        self.side = side
    }

    override func calculateArea() -> Double {
        pow(side, 2)
    }

    override func calculatePerimeter() -> Double {
        4 * side
    }
}

let rectangle = Rectangle(width: 2, height: 3)
let circle = Circle(radius: 3)
let square = Square(side: 5)

var shapes: [Shape] = [rectangle, circle, square]

var area: Double = 0
var perimeter: Double = 0

for shape in shapes {
    area += shape.calculateArea()
    perimeter += shape.calculatePerimeter()
}

print("Общая площадь = \(area)")
print("Общий периметр = \(perimeter)")

// Задача 2
func findIndexWithGenerics<T: Equatable>(valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfDouble = [1.1, 2.2, 3.3, 4.4, 5.5]
let arrayOfInt = [1, 2, 3, 4, 5]

if let foundIndexOne = findIndexWithGenerics(valueToFind: "кот", in: arrayOfString) {
    print("Индекс строки: \(foundIndexOne)")
}

if let foundIndexTwo = findIndexWithGenerics(valueToFind: 2.2, in: arrayOfDouble) {
    print("Индекс дробного числа: \(foundIndexTwo)")
}

if let foundIndexThree = findIndexWithGenerics(valueToFind: 3, in: arrayOfInt) {
    print("Индекс целого числа: \(foundIndexThree)")
}
